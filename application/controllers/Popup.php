<?php

Class  Popup extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('event_birthday_model');
    }

    function index()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        // neu ma co du lieu post len thi kiem tra
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Họ và tên', 'required');
            $this->form_validation->set_rules('email', 'Địa chỉ Email', 'required|valid_email|callback_check_email');
            $this->form_validation->set_rules('phone', 'Số điện thoại', 'required|numeric');

            // nhập liệu chính xác
            if ($this->form_validation->run()) {
                // them vao csdl
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $message = 'Đăng ký nhận quà thành công';
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'create_at' => date('Y-m-d'),
                    'message' => $message
                );
                $this->event_birthday_model->create($data);
            }
        }
        $this->data['temp'] = 'home/popup/index';
        $this->load->view('home/popup/index');
    }
    function check_email()
    {
        $email = $this->input->post('email');
        $where = array(
            'email' => $email
        );
        $check = true;
        // kiêm tra xem email đã tồn tại chưa
        if ($check && $this->event_birthday_model->check_exists($where)) {
            // trả về thông báo lỗi
            $this->form_validation->set_message(__FUNCTION__, 'Địa chỉ Email này đã được đăng ký. Vui lòng chọn một địa chỉ Email khác !');
            return false;
        }
        return true;
    }
}
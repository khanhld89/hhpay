<?php

class Admin extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
    }

    function index()
    {

        $this->load->library('pagination');

        $total_rows = $this->admin_model->get_total();
        $config = array();
        $config['base_url'] = base_url('admin/admin/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 15;
        $config['uri_segment'] = 4;

        // Khoi tao phan trang
        $this->pagination->initialize($config);
        $segment = $this->uri->segment(4);
        $segment = intval($segment);
        $input = array();
        $input['limit'] = array($config['per_page'], $segment);

        // kiem tra co thuc hien loc du lieu hay khong
        $id = $this->input->get('id');
        $id = intval($id);
        $input['where'] = array();
        if ($id > 0) {
            $input['where']['id'] = $id;
        }
        $username = $this->input->get('username');
        if ($username) {
            $input['like'] = array('username', $username);
        }
        $email = $this->input->get('email');
        if ($email) {
            $input['like'] = array('email', $email);
        }
        $phone = $this->input->get('phone');
        if ($phone) {
            $input['like'] = array('phone', $phone);
        }


        $list = $this->admin_model->get_list($input);



        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;

        $this->data['action'] = current_url();
        $this->data['total'] = $total_rows;
        $this->data['list'] = $list;
        $this->data['temp'] = 'admin/admin/index';
        $this->load->view('admin/layout', $this->data);
    }

    /*
     * Kiểm tra username đã tồn tại chưa
     */
    function check_username()
    {
        //$action = $this->uri->rsegment(2);
        $username = $this->input->post('username');
        $where = array(
            'username' => $username
        );
        $check = true;

        /*        if ($action = 'edit') {
                    $info = $this->data['info'];
                    if ($info->username != $username) {
                        $check = false;
                    }
                }*/

        // kiêm tra xem username đã tồn tại chưa
        if ($check && $this->admin_model->check_exists($where)) {
            // trả về thông báo lỗi
            $this->form_validation->set_message(__FUNCTION__, 'Tài khoản đã tồn tại');
            return false;
        }
        return true;
    }

    function add()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        // neu ma co du lieu post len thi kiem tra
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[3]');
            $this->form_validation->set_rules('username', 'UserName', 'required|callback_check_username');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'numeric');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            $this->form_validation->set_rules('re_password', 'Reply Password', 'matches[password]');

            // nhập liệu chính xác
            if ($this->form_validation->run()) {
                // them vao csdl
                $name = $this->input->post('name');
                $username = $this->input->post('username');
                $email = $this->input->post('email');
                $address = $this->input->post('address');
                $phone = $this->input->post('phone');
                $password = $this->input->post('password');
                $create_at = now();
                $update_at = now();
                $data = array(
                    'name' => $name,
                    'username' => strtolower($username),
                    'email' => $email,
                    'address' => $address,
                    'phone' => $phone,
                    'password' => md5($password),
                    'create_at' => $create_at,
                    'update_at' => $update_at,
                );

                if ($this->admin_model->create($data)) {
                    // tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Thêm thất bại');
                }
                // chuyen tới trang danh sách quản trị viên
                redirect(admin_url('admin'));
            }
        }
        $this->data['temp'] = 'admin/admin/add';
        $this->load->view('admin/layout', $this->data);
    }

    function edit()
    {
        //lay id cua quan tri vien can chinh sua
        $id = $this->uri->rsegment('3');
        $id = intval($id);


        //lay thong tin cua quan trị viên
        $info = $this->admin_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại quản trị viên');
            redirect(admin_url('admin'));
        }
        //load thu vien validation
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'required|numeric');
            $this->form_validation->set_rules('address', 'Address', 'required');

            $password = $this->input->post('password');
            if ($password) {
                $this->form_validation->set_rules('password', 'PassWord', 'required|min_length[6]');
                $this->form_validation->set_rules('re_password', 'Reply PassWord', 'matches[password]');
            }
            if ($this->form_validation->run()) {
                //them vao csdl
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $address = $this->input->post('address');
                $update_at = now();

                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,

                );
                //neu ma thay doi mat khau thi moi gan du lieu
                if ($password) {
                    $data['password'] = md5($password);
                }

                if ($this->admin_model->update($id, $data)) {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Data updated successfully');
                } else {
                    $this->session->set_flashdata('message', 'Failure to update data');
                }
                //chuyen tới trang danh sách quản trị viên
                redirect(admin_url('admin'));
            }
        }
        $this->data['action'] = current_url();
        $this->data['info'] = $info;
        $this->data['temp'] = 'admin/admin/edit';
        $this->load->view('admin/layout', $this->data);
    }

    function delete()
    {
        $id = $this->uri->rsegment('3');
        $id = intval($id);
        // lay thong tin cua quan tri vien
        $info = $this->admin_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại quản trị viên');
            redirect(admin_url('admin'));
        }
        // thuc hiện xóa
        $this->admin_model->delete($id);

        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('admin'));
    }

    private function _del($id)
    {
        $info = $this->admin_model->get_info($id);
        if (!$info) {

            $this->session->set_flashdata('message', 'không tồn tại thông tin cần xóa');
            redirect(admin_url('admin'));
        }

        $this->admin_model->delete($id);

    }

    function delete_all()
    {
        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            $this->_del($id);
        }
    }
    function logout()
    {
        if ($this->session->userdata('login')) {
            $this->session->unset_userdata('login');
        }
        redirect(admin_url('login'));
    }
}




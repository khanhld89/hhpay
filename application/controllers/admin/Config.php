<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Config extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        // Tai cac file thanh phan
        $this->load->model('config_model');
    }

    function index()
    {
        $id = 1;
        $info= $this->config_model->get_info($id);
        $this->data['info'] = $info;

        // load thư viện validate dữ liệu
        $this->load->library('form_validation');
        $this->load->helper('form');

        // neu ma co du lieu post len thi kiem tra
        if ($this->input->post()) {
            $this->form_validation->set_rules('url_home', 'Trang Chủ', 'required');
            $this->form_validation->set_rules('title', 'Tiêu đề', 'required');
            $this->form_validation->set_rules('name', 'Biệt Danh', 'required');
            $this->form_validation->set_rules('phone', 'Điện thoại hỗ trợ', 'required');
            $this->form_validation->set_rules('address', 'Địa chỉ', 'required');
            $this->form_validation->set_rules('email', 'Địa chỉ email', 'required');
            $this->form_validation->set_rules('meta_key', 'meta_keyword', 'required');
            $this->form_validation->set_rules('meta_des', 'meta_description', 'required');

            // nhập liệu chính xác
            if ($this->form_validation->run()) {
                // luu du lieu can them
                $data = array(
                    'url_home' => $this->input->post('url_home'),
                    'title' => $this->input->post('title'),
                    'meta_key' => $this->input->post('meta_key'),
                    'meta_des' => $this->input->post('meta_des'),
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'phone_all' => $this->input->post('phone_all'),
                    'fanpage' => $this->input->post('fanpage'),
                    'address' => $this->input->post('address'),
                    'email' => $this->input->post('email'),
                    'meta_author' => $this->input->post('meta_author'),
                    'google_analytics' => $this->input->post('google_analytics'),
                );

                // them moi vao csdl
                if ($this->config_model->update($id, $data)) {
                    // tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Không cập nhật được');
                }
                // chuyen tới trang danh sách
                redirect(admin_url('home'));
            }
        }
        $this->data['temp'] = 'admin/config/index';
        $this->load->view('admin/layout', $this->data);
    }
}
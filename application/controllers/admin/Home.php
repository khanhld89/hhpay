<?php

class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('slide_model');
        $this->load->model('admin_model');
        $this->load->model('dauso_model');
        $this->load->model('doithe_model');
        $this->load->model('event_birthday_model');
        $this->load->model('event_ketnoi_model');
        $this->load->model('topup_model');
    }

    function index()
    {

        $this->data['total_slide'] = $this->slide_model->get_total();
        $this->data['total_admin'] = $this->admin_model->get_total();
        $this->data['total_dauso'] = $this->dauso_model->get_total();
        $this->data['total_doithe'] = $this->doithe_model->get_total();
        $this->data['total_brithday'] = $this->event_birthday_model->get_total();
        $this->data['total_ketnoi'] = $this->event_ketnoi_model->get_total();
        $this->data['total_topup'] = $this->topup_model->get_total();

        $data['temp'] = 'admin/home/index';
        $this->load->view('admin/layout', $data);
    }
}
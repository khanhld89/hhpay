<?php

class Slide extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Slide_model');
    }

    function index()
    {
        $this->load->library('pagination');

        $total_rows = $this->Slide_model->get_total();
        $config = array();
        $config['base_url'] = base_url('admin/slide/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 15;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $config['last_link'] = "Trang cuối";
        $config['first_link'] = "Trang đầu";

        // Khoi tao phan trang
        $this->pagination->initialize($config);
        $input = array();
        $segment = $this->uri->segment(4);
        $segment = intval($segment);
        $input = array();
        $input['limit'] = array($config['per_page'], $segment);


        // kiem tra co thuc hien loc du lieu hay khong
        $id = $this->input->get('id');
        $id = intval($id);
        $input['where'] = array();
        if ($id > 0) {
            $input['where']['id'] = $id;
        }
        $name = $this->input->get('name');
        if ($name) {
            $input['like'] = array('name', $name);
        }

        $input['order'] = array('sort_order', 'ASC');
        $list = $this->Slide_model->get_list($input);
        $this->data['list'] = $list;
        $this->data['action'] = current_url();
        $this->data['total'] = $total_rows;
        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;


        $this->data['temp'] = 'admin/slide/index';
        $this->load->view('admin/layout', $this->data);
    }

    function add()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        // neu ma co du lieu post len thi kiem tra
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Tên nhà mạng', 'required');
            $this->form_validation->set_rules('sort_order', 'Thứ tự hiển thị', 'required|numeric');

            // nhập liệu chính xác
            if ($this->form_validation->run()) {

                // lay ten file anh minh hoa duoc update len
                $this->load->library('upload_library');
                $upload_path = 'public/upload/slide';
                $upload_data = $this->upload_library->upload($upload_path, 'image');
                $image_link = '';
                if (isset($upload_data['file_name'])) {
                    $image_link = $upload_data['file_name'];
                }

                // them vao csdl
                $name = $this->input->post('name');
                $link = $this->input->post('link');
                $desc = $this->input->post('desc');
                $sort_order = $this->input->post('sort_order');
                $data = array(
                    'name' => $name,
                    'link' => $link,
                    'sort_order' => $sort_order,
                    'image' => $image_link,
                    'desc' => $desc,
                    'create_at' => now(),
                );

                if ($this->Slide_model->create($data)) {
                    // tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thất bại');
                }
                // chuyen tới trang danh sách quản trị viên
                redirect(admin_url('slide'));
            }
        }
        $this->data['temp'] = 'admin/slide/add';
        $this->load->view('admin/layout', $this->data);
    }

    function edit()
    {
        //lay id  can chinh sua
        $id = $this->uri->rsegment('3');
        $id = intval($id);


        //lay thong tin cua quan trị viên
        $info = $this->Slide_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại bản ghi cần chỉnh sửa');
            redirect(admin_url('slide'));
        }
        //load thu vien validation
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Tên nhà mạng', 'required');
            $this->form_validation->set_rules('sort_order', 'Thứ tự hiển thị', 'required|numeric');

            if ($this->form_validation->run()) {
                //them vao csdl
                // lay ten file anh minh hoa duoc update len
                $this->load->library('upload_library');
                $upload_path = 'public/upload/slide';
                $upload_data = $this->upload_library->upload($upload_path, 'image');
                $image_link = '';
                if (isset($upload_data['file_name'])) {
                    $image_link = $upload_data['file_name'];
                }
                // them vao csdl
                $name = $this->input->post('name');
                $link = $this->input->post('link');
                $desc = $this->input->post('desc');
                $sort_order = $this->input->post('sort_order');
                $data = array(
                    'name' => $name,
                    'link' => $link,
                    'sort_order' => $sort_order,
                    'desc' => $desc,
                );
                if ($image_link != '') {
                    $data['image'] = $image_link;
                }

                if ($this->Slide_model->update($id, $data)) {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Cập nhật thất bại');
                }
                redirect(admin_url('slide'));
            }
        }
        $this->data['action'] = current_url();
        $this->data['info'] = $info;
        $this->data['temp'] = 'admin/slide/edit';
        $this->load->view('admin/layout', $this->data);
    }

    function delete()
    {
        $id = $this->uri->rsegment('3');
        $id = intval($id);
        // lay thong tin cua quan tri vien
        $info = $this->Slide_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn bản ghi');
            redirect(admin_url('slide'));
        }
        // thuc hiện xóa
        $this->Slide_model->delete($id);
        // xoa cac anh cua san pham
        $image = 'public/upload/slide/' . $info->image;
        if (file_exists($image)) {
            unlink($image);
        }
        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('slide'));
    }

    function delete_all()
    {
        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            $this->_del($id);
        }
    }

    private function _del($id)
    {
        $info = $this->Slide_model->get_info($id);
        if (!$info) {

            $this->session->set_flashdata('message', 'không tồn tại bản ghi này');
            redirect(admin_url('contact'));
        }
        $this->Slide_model->delete($id);
        // xoa cac anh cua san pham
        $image = 'public/upload/slide/' . $info->image;
        if (file_exists($image)) {
            unlink($image);
        }
    }
}




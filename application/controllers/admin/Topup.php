<?php

class topup extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Topup_model');
    }

    function index()
    {
        $this->load->library('pagination');

        $total_rows = $this->Topup_model->get_total();
        $config = array();
        $config['base_url'] = base_url('admin/topup/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 15;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $config['last_link'] = "Trang cuối";
        $config['first_link'] = "Trang đầu";

        // Khoi tao phan trang
        $this->pagination->initialize($config);
        $input = array();
        $segment = $this->uri->segment(4);
        $segment = intval($segment);
        $input = array();
        $input['limit'] = array($config['per_page'], $segment);


        // kiem tra co thuc hien loc du lieu hay khong
        $id = $this->input->get('id');
        $id = intval($id);
        $input['where'] = array();
        if ($id > 0) {
            $input['where']['id'] = $id;
        }
        $name = $this->input->get('name');
        if ($name) {
            $input['like'] = array('name', $name);
        }
        $ty_le = $this->input->get('ty_le');
        if ($ty_le) {
            $input['like'] = array('ty_le', $ty_le);
        }


        $input['order'] = array('sort_order', 'ASC');
        $list = $this->Topup_model->get_list($input);
        $this->data['list'] = $list;
        $this->data['action'] = current_url();
        $this->data['total'] = $total_rows;
        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;


        $this->data['temp'] = 'admin/topup/index';
        $this->load->view('admin/layout', $this->data);
    }

    function add()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        // neu ma co du lieu post len thi kiem tra
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Tên nhà mạng', 'required');
            $this->form_validation->set_rules('ty_le', 'Tỷ lệ', 'required');
            $this->form_validation->set_rules('sort_order', 'Thứ tự hiển thị', 'required|numeric');

            // nhập liệu chính xác
            if ($this->form_validation->run()) {
                // them vao csdl
                $name = $this->input->post('name');
                $ty_le = $this->input->post('ty_le');
                $sort_order = $this->input->post('sort_order');
                $data = array(
                    'name' => $name,
                    'ty_le' => $ty_le,
                    'sort_order' => $sort_order,
                    'create_at' => now(),
                );

                if ($this->Topup_model->create($data)) {
                    // tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thất bại');
                }
                // chuyen tới trang danh sách quản trị viên
                redirect(admin_url('topup'));
            }
        }
        $this->data['temp'] = 'admin/topup/add';
        $this->load->view('admin/layout', $this->data);
    }

    function edit()
    {
        //lay id  can chinh sua
        $id = $this->uri->rsegment('3');
        $id = intval($id);


        //lay thong tin cua quan trị viên
        $info = $this->Topup_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại bản ghi cần chỉnh sửa');
            redirect(admin_url('topup'));
        }
        //load thu vien validation
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Tên nhà mạng', 'required');
            $this->form_validation->set_rules('ty_le', 'Tỷ lệ', 'required');
            $this->form_validation->set_rules('sort_order', 'Thứ tự hiển thị', 'required|numeric');

            if ($this->form_validation->run()) {
                //them vao csdl
                $name = $this->input->post('name');
                $ty_le = $this->input->post('ty_le');
                $sort_order = $this->input->post('sort_order');
                $data = array(
                    'name' => $name,
                    'ty_le' => $ty_le,
                    'sort_order' => $sort_order,
                );


                if ($this->Topup_model->update($id, $data)) {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Cập nhật thất bại');
                }
                redirect(admin_url('topup'));
            }
        }
        $this->data['action'] = current_url();
        $this->data['info'] = $info;
        $this->data['temp'] = 'admin/topup/edit';
        $this->load->view('admin/layout', $this->data);
    }

    function delete()
    {
        $id = $this->uri->rsegment('3');
        $id = intval($id);
        // lay thong tin cua quan tri vien
        $info = $this->Topup_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn bản ghi');
            redirect(admin_url('topup'));
        }
        // thuc hiện xóa
        $this->Topup_model->delete($id);

        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('topup'));
    }

    function delete_all()
    {
        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            $this->_del($id);
        }
    }

    private function _del($id)
    {
        $info = $this->Topup_model->get_info($id);
        if (!$info) {

            $this->session->set_flashdata('message', 'không tồn tại bản ghi này');
            redirect(admin_url('topup'));
        }

        $this->Topup_model->delete($id);

    }
}




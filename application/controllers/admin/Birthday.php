<?php

class Birthday extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('event_birthday_model');
    }

    function index()
    {
        $this->load->library('pagination');

        $total_rows = $this->event_birthday_model->get_total();
        $config = array();
        $config['base_url'] = base_url('admin/birthday/index');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 15;
        $config['uri_segment'] = 4;
        $config['next_link'] = "Trang kế tiếp";
        $config['prev_link'] = "Trang trước";
        $config['last_link'] = "Trang cuối";
        $config['first_link'] = "Trang đầu";

        // Khoi tao phan trang
        $this->pagination->initialize($config);
        $input = array();
        $segment = $this->uri->segment(4);
        $segment = intval($segment);
        $input = array();
        $input['limit'] = array($config['per_page'], $segment);


        // kiem tra co thuc hien loc du lieu hay khong
        $id = $this->input->get('id');
        $id = intval($id);
        $input['where'] = array();
        if ($id > 0) {
            $input['where']['id'] = $id;
        }
        $name = $this->input->get('name');
        if ($name) {
            $input['like'] = array(
                'name',
                $name
            );
        }
        $create_at = $this->input->get('create_at');
        if ($create_at) {
            $input['like'] = array(
                'create_at',
                $create_at
            );
        }
        $email = $this->input->get('email');
        if ($email) {
            $input['like'] = array(
                'email',
                $email
            );
        }
        $phone = $this->input->get('phone');
        if ($phone) {
            $input['like'] = array(
                'phone', $phone
            );
        }


        $list = $this->event_birthday_model->get_list($input);
        $this->data['list'] = $list;
        $this->data['action'] = current_url();
        $this->data['total'] = $total_rows;
        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;


        $this->data['temp'] = 'admin/birthday/index';
        $this->load->view('admin/layout', $this->data);
    }

    function add()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        // neu ma co du lieu post len thi kiem tra
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Họ và tên', 'required|min_length[3]');
            $this->form_validation->set_rules('email', 'Địa chỉ Email', 'required|valid_email|callback_check_email');
            $this->form_validation->set_rules('phone', 'Số điện thoại', 'required|numeric');

            // nhập liệu chính xác
            if ($this->form_validation->run()) {
                // them vao csdl
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'create_at' => date('Y-m-d')
                );

                if ($this->event_birthday_model->create($data)) {
                    // tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thất bại');
                }
                // chuyen tới trang danh sách quản trị viên
                redirect(admin_url('birthday'));
            }
        }
        $this->data['temp'] = 'admin/birthday/add';
        $this->load->view('admin/layout', $this->data);
    }

    function edit()
    {
        //lay id  can chinh sua
        $id = $this->uri->rsegment('3');
        $id = intval($id);


        //lay thong tin cua quan trị viên
        $info = $this->event_birthday_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn tại bản ghi cần chỉnh sửa');
            redirect(admin_url('birthday'));
        }
        //load thu vien validation
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Họ và tên', 'required|min_length[8]');
            $this->form_validation->set_rules('email', 'Địa chỉ Email', 'required|valid_email');
            $this->form_validation->set_rules('phone', 'Số điện thoại', 'required|numeric');


            if ($this->form_validation->run()) {
                //them vao csdl
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');

                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                );

                if ($this->event_birthday_model->update($id, $data)) {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                } else {
                    $this->session->set_flashdata('message', 'Cập nhật thất bại');
                }
                redirect(admin_url('birthday'));
            }
        }
        $this->data['action'] = current_url();
        $this->data['info'] = $info;
        $this->data['temp'] = 'admin/birthday/edit';
        $this->load->view('admin/layout', $this->data);
    }

    function delete()
    {
        $id = $this->uri->rsegment('3');
        $id = intval($id);
        // lay thong tin cua quan tri vien
        $info = $this->event_birthday_model->get_info($id);
        if (!$info) {
            $this->session->set_flashdata('message', 'Không tồn bản ghi');
            redirect(admin_url('birthday'));
        }
        // thuc hiện xóa
        $this->event_birthday_model->delete($id);

        $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
        redirect(admin_url('birthday'));
    }

    function delete_all()
    {
        $ids = $this->input->post('ids');
        foreach ($ids as $id) {
            $this->_del($id);
        }
    }

    private function _del($id)
    {
        $info = $this->event_birthday_model->get_info($id);
        if (!$info) {

            $this->session->set_flashdata('message', 'không tồn tại liên hệ này');
            redirect(admin_url('birthday'));
        }

        $this->event_birthday_model->delete($id);

    }
    function check_email()
    {
        $email = $this->input->post('email');
        $where = array(
            'email' => $email
        );
        $check = true;
        // kiêm tra xem email đã tồn tại chưa
        if ($check && $this->event_birthday_model->check_exists($where)) {
            // trả về thông báo lỗi
            $this->form_validation->set_message(__FUNCTION__, 'Địa chỉ Email này đã được đăng ký. Vui lòng chọn một địa chỉ Email khác !');
            return false;
        }
        return true;
    }

    public function downloadExcel()
    {
        $list = $this->event_birthday_model->get_list();


        $this->load->library("excel");
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("");
        $objPHPExcel->getProperties()->setLastModifiedBy("");
        $objPHPExcel->getProperties()->setTitle("");
        $objPHPExcel->getProperties()->setSubject("");
        $objPHPExcel->getProperties()->setDescription("");

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Mã ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Họ Tên');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Số điện thoại');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Địa chỉ Email');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Thông điệp');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Ngày đăng ký');

        $level = 2;
        $i = 1;
        foreach ($list as $key => $value) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $level, $i);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $level, $value->id);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $level, $value->name);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $level, $value->phone);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $level, $value->email);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $level, $value->message);
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $level, date("d-m-Y", strtotime($value->create_at)));
            $i++;
            $level++;
        }
        $filename = 'Event-Birthday-' . date("d-m-Y") . '.xlsx';
        $objPHPExcel->getActiveSheet()->setTitle("Du-Lieu");

        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("php://output");
    }
}




<?php

Class MY_Controller extends CI_Controller
{
    public $data = array();

    function __construct()
    {
        parent::__construct();
        //xu ly cac du lieu khi truy cap vao trang admin
        $action = $this->uri->segment(1);
        strtolower($action);
        if ($action == 'admin') {
            $this->_check_login();
        }else{
            $this->load->model('config_model');
            $config = $this->config_model->get_info(1);
            // gửi sang view
            $this->data['config'] = $config;
        }
    }
    /*
     * Kiem tra trang thai dang nhap cua admin
     */
    private function _check_login()
    {
        $controller = $this->uri->rsegment('1');
        $controller = strtolower($controller);

        $login = $this->session->userdata('login');


        //neu ma chua dang nhap,ma truy cap 1 controller khac login
        if (!$login && $controller != 'login') {
            redirect(admin_url('login'));
        }
        //neu ma admin da dang nhap thi khong cho phep vao trang login nua.
        if ($login && $controller == 'login') {
            redirect(admin_url('home'));
        }
    }
}



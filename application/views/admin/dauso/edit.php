<!-- head -->
<?php $this->load->view('admin/dauso/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Sửa thông tin đầu số <?php  echo $info->dau_so ?></h6>
        </div>

        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="<?php echo $action ?>">
            <fieldset>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Đầu số:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo $info->dau_so ?>" name="dau_so"></span> <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('dau_so') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Cước phí:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_phone" value="<?php echo $info->cuoc_phi ?>" name="cuoc_phi"></span> <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('cuoc_phi') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Loại:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" value="<?php echo $info->type ?>" id="param_email" name="type"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('type') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Tỷ lệ:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->ty_le ?>" id="param_email" name="ty_le"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('ty_le') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Thứ tự hiển thị:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->sort_order ?>" id="param_email" name="sort_order"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('sort_order') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formSubmit">
                    <input type="submit" class="redB" value="Cập nhật">
                </div>
            </fieldset>
        </form>

    </div>
</div>

<!-- head -->
<?php $this->load->view('admin/config/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">

    <!-- Form -->
    <form enctype="multipart/form-data" method="post" action="config" id="form" class="form">
        <fieldset>
            <div class="widget">
                <div class="title">
                    <img class="titleIcon"
                         src="<?php echo public_url('admin') ?>/images/icons/dark/add.png">
                    <h6>Cấu hình website</h6>
                </div>

                <ul class="tabs">
                    <li class="activeTab"><a href="#tab1">Thông tin chung</a></li>
                    <li class=""><a href="#tab2">Seo onpage</a></li>
                </ul>

                <div class="tab_container">
                    <div class="tab_content pd0" id="tab1" style="display: block;">
                        <div class="formRow">
                            <label for="param_url" class="formLeft">Biệt Danh :</label>
                            <div class="formRight">
								<span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->name ?>" name="name"></span>
                                <span class="autocheck" name="name_autocheck"></span>
                                <div class="clear error"><?php echo form_error('name') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="formRow">
                            <label for="param_title" class="formLeft">Trang chủ :</label>
                            <div class="formRight">
								<span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->url_home ?>" name="url_home"></span>
                                <span class="autocheck" name="name_autocheck"></span>
                                <div class="clear error"><?php echo form_error('url_home') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Số điện thoại hỗ trợ:</label>
                            <div class="formRight">
								<span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->phone ?>" name="phone"></span>
                                <span class="autocheck" name="phone"></span>
                                <div class="clear error"><?php echo form_error('phone') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Tất cả số điện thoại:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->phone_all ?>" name="phone_all"></span>
                                <span class="autocheck" name="phone"></span>
                                <div class="clear error"><?php echo form_error('phone_all') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Fanapge:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->fanpage ?>" name="fanpage"></span>
                                <span class="autocheck" name="fanpage"></span>
                                <div class="clear error"><?php echo form_error('fanpage') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Địa chỉ:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->address ?>" name="address"></span>
                                <span class="autocheck" name="address"></span>
                                <div class="clear error"><?php echo form_error('address') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Địa chỉ email:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->email ?>" name="email"></span>
                                <span class="autocheck" name="email"></span>
                                <div class="clear error"><?php echo form_error('email') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="formRow hide"></div>
                    </div>

                    <div class="tab_content pd0" id="tab2" style="display: block;">
                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Tiêu đề:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->title ?>" name="title"></span>
                                <span class="autocheck" name="title"></span>
                                <div class="clear error"><?php echo form_error('title') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Google Analytics:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->google_analytics ?>" name="google_analytics"></span>
                                <span class="autocheck" name="google_analytics"></span>
                                <div class="clear error"><?php echo form_error('google_analytics') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_description" class="formLeft">Meta Author:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input type="text" _autocheck="true" id="param_title" value="<?php echo $info->meta_author ?>" name="meta_author"></span>
                                <span class="autocheck" name="meta_author"></span>
                                <div class="clear error"><?php echo form_error('meta_author') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_key" class="formLeft">Meta keywords(Từ khóa):</label>
                            <div class="formRight">
                                <span class="oneTwo"><textarea cols="" rows="4" _autocheck="true" id="param_meta_keyword" name="meta_key"><?php echo $info->meta_key ?></textarea></span>
                                <span class="autocheck" name="meta_keyword_autocheck"></span>
                                <div class="clear error"><?php echo form_error('meta_key') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="formRow">
                            <label for="param_meta_key" class="formLeft">Meta Description (Mô tả):</label>
                            <div class="formRight">
                                <span class="oneTwo"><textarea cols="" rows="4" _autocheck="true" id="param_meta_keyword" name="meta_des"><?php echo $info->meta_des ?></textarea></span>
                                <span class="autocheck" name="meta_keyword_autocheck"></span>
                                <div class="clear error"><?php echo form_error('meta_des') ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <!-- End tab_container-->

                <div class="formSubmit">
                    <input type="submit" class="redB" value="Cập nhật"> <input
                            type="reset" class="basic" value="Hủy bỏ">
                </div>
                <div class="clear"></div>
            </div>
        </fieldset>
    </form>
</div>
<div class="clear mt30"></div>

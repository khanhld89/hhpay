<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Cài đặt</h5>
            <span>Quản lý cấu hình website</span>
        </div>

        <div class="clear"></div>
    </div>
</div>

<script type="text/javascript">
    (function($)
    {
        $(document).ready(function()
        {
            var main = $('#form');

            // Tabs
            main.contentTabs();
        });
    })(jQuery);
</script>

<!-- head -->
<?php $this->load->view('admin/doithe/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới dịch vụ đầu số</h6>
        </div>


        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="add">
            <fieldset>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Tên nhà mạng:<span class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo set_value('dau_so') ?>" name="name" placeholder="Nhập vào tên cần thêm"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left">
                            <input value="<?php echo set_value('image') ?>" type="file" name="image" id="image" size="25">
                        </div>
                        <div class="clear error" name="image_error"><?php echo form_error('image') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_email" class="formLeft">Tỷ lệ:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo set_value('ty_le')?>" id="param_price" name="ty_le" placeholder="Nhập vào tỷ lệ cần thêm"></span>
                        <div class="clear error"><?php echo form_error('ty_le') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_email" class="formLeft">Thứ tự hiển thị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo set_value('sort_order')?>" id="param_price" name="sort_order" placeholder="Nhập vào thứ tự hiển thị cần thêm"></span>
                        <div class="clear error"><?php echo form_error('sort_order') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
                <div class="formSubmit">
                    <input type="submit" class="redB" value="Thêm mới">
                </div>

            </fieldset>
        </form>

    </div>
</div>
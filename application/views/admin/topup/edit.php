<!-- head -->
<?php $this->load->view('admin/topup/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Sửa thông tin TopUp <?php  echo $info->name ?></h6>
        </div>

        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="<?php echo $action ?>">
            <fieldset>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Tên nhà mạng:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo $info->name ?>" name="name"></span> <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formRow">
                    <label for="param_username" class="formLeft">Tỷ lệ:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->ty_le ?>" id="param_email" name="ty_le"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('ty_le') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Thứ tự hiển thị:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->sort_order ?>" id="param_email" name="sort_order"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('sort_order') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formSubmit">
                    <input type="submit" class="redB" value="Cập nhật">
                </div>
            </fieldset>
        </form>

    </div>
</div>

<div id="leftSide" style="padding-top: 30px;">
    <div class="sideProfile">
        <a href="#" title="" class="profileFace"><img
                    src="<?php echo public_url('admin') ?>/images/user.png" width="40"></a>
        <span>Xin chào: <strong>  <?php ?> </strong></span>
        <span>Admin</span>
        <div class="clear"></div>
    </div>
    <div class="sidebarSep"></div>

    <ul id="menu" class="nav">

        <li class="home"><a href="<?php echo admin_url() ?>" class="active" id="current">
                <span>Trang Quản Trị</span> <strong></strong>
            </a></li>

        <li class="account"><a href="" class="exp inactive"> <span>Quản lý User</span> <strong>1</strong>
            </a>

            <ul style="display: none;" class="sub">
                <li><a href="<?php echo admin_url('admin') ?>">Ban Quản Trị</a></li>
            </ul>
        </li>
        <li class="support"><a href="" class="exp inactive"> <span>Sự kiện</span> <strong>2</strong>
            </a>
            <ul style="display: none;" class="sub">
                <li><a href="<?php echo admin_url('birthday') ?>">Nhận quà sinh nhật</a></li>
                <li><a href="<?php echo admin_url('ketnoi') ?>">Nhận quà kết nối ngay</a></li>
            </ul>
        </li>
        <li class="forms"><a href="" class="exp inactive"> <span>Quản lý dịch vụ</span> <strong>3</strong>
            </a>
            <ul style="display: none;" class="sub">
                <li><a href="<?php echo admin_url('doithe') ?>">Dịch vụ đổi thẻ</a></li>
                <li><a href="<?php echo admin_url('dauso') ?>">Dịch vụ đầu số</a></li>
                <li><a href="<?php echo admin_url('topup') ?>">Dịch vụ TopUp</a></li>
            </ul>
        </li>
        <li class="widgets"><a href="" class="exp inactive"> <span>Quản lý nội dung</span> <strong>1</strong>
            </a>
            <ul style="display: none;" class="sub">
                <li><a href="<?php echo admin_url('slide') ?>">Quản lý slide</a></li>
            </ul>
        </li>

        <li class="tran"><a href="" class="exp inactive"> <span>Setup</span> <strong>1</strong> </a>
            <ul style="display: none;" class="sub">
                <li><a href="<?php echo admin_url('config') ?>"> Config Website</a></li>
            </ul>
        </li>
    </ul>
</div>
<div class="clear"></div>

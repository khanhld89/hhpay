<!-- head -->
<?php $this->load->view('admin/slide/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới dịch vụ đầu số</h6>
        </div>


        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="add">
            <fieldset>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Tên banner:<span class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo set_value('name') ?>" name="name" placeholder="Nhập vào tên cần thêm"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left">
                            <input value="<?php echo set_value('image') ?>" type="file" name="image" id="image" size="25">
                        </div>
                        <div class="clear error" name="image_error"><?php echo form_error('image') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_email" class="formLeft">Đường dẫn:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo set_value('link')?>" id="param_price" name="link" placeholder="Nhập vào đường dẫn cần thêm"></span>
                        <div class="clear error"><?php echo form_error('link') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_email" class="formLeft">Mô tả:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><textarea type="text" value="<?php echo set_value('desc')?>" id="param_price" name="desc" placeholder="Nhập vào mô tả cần thêm"> </textarea></span>
                        <div class="clear error"><?php echo form_error('desc') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_email" class="formLeft">Thứ tự hiển thị:<span class="req">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo set_value('sort_order')?>" id="param_price" name="sort_order" placeholder="Nhập vào thứ tự hiển thị cần thêm"></span>
                        <div class="clear error"><?php echo form_error('sort_order') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
                <div class="formSubmit">
                    <input type="submit" class="redB" value="Thêm mới">
                </div>

            </fieldset>
        </form>

    </div>
</div>
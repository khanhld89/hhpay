<!-- head -->
<?php $this->load->view('admin/slide/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Sửa thông tin slide <?php  echo $info->name ?></h6>
        </div>

        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="<?php echo $action ?>">
            <fieldset>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Tên slide:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo $info->name ?>" name="name"></span> <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="left">
                            <input type="file" name="image" id="image" size="25"> <img src="<?php echo public_url('upload/slide/' . $info->image) ?>" style="width: 100px;">
                        </div>
                        <div class="clear error"><?php echo form_error('image') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Đường dẫn:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->link ?>" id="param_email" name="link"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('link') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Mô tả:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><textarea type="text" value="<?php echo $info->desc ?>" id="param_email" name="desc"><?php echo $info->desc ?></textarea></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('desc') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Thứ tự hiển thị:<span class="req"></span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->sort_order ?>" id="param_email" name="sort_order"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('sort_order') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formSubmit">
                    <input type="submit" class="redB" value="Cập nhật">
                </div>
            </fieldset>
        </form>

    </div>
</div>

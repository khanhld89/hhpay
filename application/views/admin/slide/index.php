<?php $this->load->view('admin/slide/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data); ?>
    <div class="widget">

        <div class="title">
            <span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck"/></span>
            <h6>Danh sách đầu số</h6>
            <div class="num f12"> Tổng: <b><?php echo $total ?> bản ghi</b>
            </div>
        </div>

            <table class="sTable mTable myTable" id="checkAll">
                <thead class="filter">
                <tr>
                    <td colspan="9">
                        <form method="get" id="form" action="<?php echo admin_url('slide') ?>" class="list_filter form">
                            <table width="80%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td style="width:40px;" class="label"><label for="filter_id">Mã số</label></td>
                                    <td class="item"><input type="text" style="width:55px;" id="filter_id" value="<?php echo $this->input->get('id') ?>" name="id">
                                    </td>

                                    <td style="width:40px;" class="label"><label for="filter_name">Tên:</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_name" value="<?php echo $this->input->get('name') ?>" name="name"></td>

                                    <td style="width:150px">
                                        <input type="submit" value="Lọc" class="button blueB">
                                        <input type="reset" onclick="window.location.href = '<?php echo admin_url('slide') ?>'; " value="Reset" class="basic">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                </thead>
                <thead>
                <tr>
                    <td style="width:10px;"><img src="<?php echo public_url('admin'); ?>/images/icons/tableArrows.png"/>
                    </td>
                    <td style="width: 80px;">Mã ID</td>
                    <td>Tên slide</td>
                    <td>Ảnh</td>
                    <td>Đường dẫn</td>
                    <td>Mô tả</td>
                    <td>Thứ tự hiển thị</td>
                    <td>Ngày Thêm</td>
                    <td style="width: 100px;">Hành động</td>
                </tr>
                </thead>

                <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="9">
                        <div class="list_action itemActions">
                            <a url="<?php echo admin_url('slide/delete_all') ?>" class="button blueB" id="submit" href="#submit">
                                <span style="color:white;">Delete</span>
                            </a>
                        </div>
                        <div class='pagination'>
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
                </tfoot>


                <tbody class="list_item">
                <?php foreach ($list as $row): ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?php echo $row->id; ?>"/></td>
                        <td class="textC"><?php echo $row->id ?></td>
                        <td><span title="<?php echo $row->name ?>" class="tipS"><?php echo $row->name ?></span></td>
                        <td><span title="<?php echo $row->name ?>" class="tipS"><img width="100px"  src="<?php echo public_url()?>upload/slide/<?php echo $row->image?>" alt="<?php echo $row->name?>"></span></td>
                        <td><span title="<?php echo $row->link ?>" class="tipS"><?php echo $row->link ?></span></td>
                        <td><span title="<?php echo $row->desc ?>" class="tipS"><?php echo $row->desc ?></span></td>
                        <td><span title="<?php echo $row->sort_order ?>" class="tipS"><?php echo $row->sort_order ?></span></td>
                        <td><span class="tipS"><?php echo date('d-m-Y',$row->create_at) ?>
                        <td class="option"><a href="<?php echo admin_url('slide/edit/' . $row->id) ?>" title="Edit" class="tipS "> <img src="<?php echo public_url('admin') ?>/images/icons/color/edit.png"/>
                            </a> <a href="<?php echo admin_url('slide/delete/' . $row->id) ?>" title="Delete" class="tipS verify_action"> <img src="<?php echo public_url('admin') ?>/images/icons/color/delete.png"/>
                            </a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
    </div>
</div>

<div class="clear mt30"></div>

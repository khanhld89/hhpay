<div class="titleArea">
	<div class="wrapper">
		<div class="pageTitle">
			<h5>Dashboard</h5>
			<span>Version 1.0 management system</span>
		</div>
		
		<div class="clear"></div>
	</div>
</div>

<div class="line"></div>

<div class="wrapper">
	
	<div class="widgets">
	     <!-- Stats -->
		<?php $this->load->view('admin/home/stats', $this->data); ?>
		<div class="clear"></div>
    </div>
	
</div>


<div class="clear mt30"></div>


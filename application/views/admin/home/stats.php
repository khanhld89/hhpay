<div class="oneTwo">
    <div class="widget">
        <?php $this->load->view('admin/message', $this->data); ?>
        <div class="title">
            <img src="<?php echo public_url('admin'); ?>/images/icons/dark/money.png" class="titleIcon"/>
            <h6>Statistical data</h6>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%"
               class="sTable myTable">
            <tbody>

            <tr>
                <td class="fontB blue f13">Tổng số admin</td>
                <td class="textR webStatsLink red" style="width: 120px;"> <?php echo $total_admin?></td>
            </tr>

            <tr>
                <td class="fontB blue f13">Tổng số slide </td>
                <td class="textR webStatsLink red" style="width: 120px;"><?php echo $total_slide?></td>
            </tr>

            <tr>
                <td class="fontB blue f13">Danh sách đổi thẻ</td>
                <td class="textR webStatsLink red" style="width: 120px;"><?php echo $total_doithe?></td>
            </tr>

            <tr>
                <td class="fontB blue f13">Danh sách topup</td>
                <td class="textR webStatsLink red" style="width: 120px;"><?php echo $total_topup?></td>
            </tr>

            <tr>
                <td class="fontB blue f13">Danh sách đầu số</td>
                <td class="textR webStatsLink red" style="width: 120px;"><?php echo $total_dauso ?></td>
            </tr>

            </tbody>
        </table>
    </div>
</div>

<!-- User -->
<div class="oneTwo">
    <div class="widget">
        <div class="title">
            <img src="<?php echo public_url('admin'); ?>/images/icons/dark/users.png" class="titleIcon"/>
            <h6>Statistical data</h6>
        </div>

        <table cellpadding="0" cellspacing="0" width="100%"
               class="sTable myTable">
            <tbody>

            <tr>
                <td class="fontB blue f13">Đăng ký kết nối ngay</td>
                <td class="textR webStatsLink red" style="width: 120px;"><?php echo $total_ketnoi?></td>
            </tr>

            <tr>
                <td class="fontB blue f13">Đăng ký sự kiện nhận 19.000.000</td>
                <td class="textR webStatsLink red" style="width: 120px;"><?php echo $total_brithday?></td>
            </tr>

            <tr>
                <td class="fontB blue f13">Update...</td>
                <td class="textR webStatsLink red" style="width: 120px;"></td>
            </tr>
            <tr>
                <td class="fontB blue f13">Update...</td>
                <td class="textR webStatsLink red" style="width: 120px;"></td>
            </tr>
            <tr>
                <td class="fontB blue f13">Update...</td>
                <td class="textR webStatsLink red" style="width: 120px;"></td>
            </tr>


            </tbody>
        </table>
    </div>
</div>


<?php $this->load->view('admin/admin/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">

    <?php $this->load->view('admin/message', $this->data); ?>

    <div class="widget">

        <div class="title">
            <span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck"/></span>
            <h6>List Admin</h6>
            <div class="num f12"> total: <b><?php echo $total ?></b>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="get" class="form" name="filter">

            <table class="sTable mTable myTable withCheck" id="checkAll">

                <thead class="filter">
                <tr>
                    <td colspan="9">
                        <form method="get" id="form" action="<?php echo admin_url('admin') ?>" class="list_filter form">
                            <table width="80%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td style="width:40px;" class="label"><label for="filter_id">Mã số</label></td>
                                    <td class="item"><input type="text" style="width:55px;" id="filter_id" value="<?php echo $this->input->get('id') ?>" name="id">
                                    </td>

                                    <td style="width:40px;" class="label"><label for="filter_name">Username:</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_name" value="<?php echo $this->input->get('username') ?>" name="username"></td>

                                    <td style="width:40px;" class="label"><label for="filter_email">Email</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_email" value="<?php echo $this->input->get('email') ?>" name="email"></td>

                                    <td style="width:40px;" class="label"><label for="filter_email">phone</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_email" value="<?php echo $this->input->get('phone') ?>" name="phone"></td>


                                    <td style="width:150px">
                                        <input type="submit" value="Lọc" class="button blueB">
                                        <input type="reset" onclick="window.location.href = '<?php echo admin_url('admin') ?>'; " value="Reset" class="basic">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                </thead>

                <thead>
                <tr>
                    <td style="width:10px;"><img src="<?php echo public_url('admin'); ?>/images/icons/tableArrows.png"/>
                    </td>
                    <td style="width: 80px;">ID</td>
                    <td>Full Name</td>
                    <td>User Name</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Address</td>
                    <td style="width: 100px;">Action</td>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td colspan="9">
                        <div class="list_action itemActions">
                            <a url="<?php echo admin_url('admin/delete_all') ?>" class="button blueB" id="submit"
                               href="#submit">
                                <span style="color:white;">Delete All</span>
                            </a>
                        </div>
                        <div class='pagination'>
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
                </tfoot>


                <tbody>
                <?php foreach ($list as $row): ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?php echo $row->id; ?>"/></td>
                        <td class="textC"><?php echo $row->id ?></td>
                        <td><span title="<?php echo $row->name ?>" class="tipS"><?php echo $row->name ?></span></td>
                        <td><span title="<?php echo $row->username ?>" class="tipS"><?php echo $row->username ?></span></td>
                        <td><span title="<?php echo $row->email ?>" class="tipS"><?php echo $row->email ?></span></td>
                        <td><span title="<?php echo $row->phone ?>" class="tipS"><?php echo $row->phone ?></span></td>
                        <td><span title="<?php echo $row->address ?>" class="tipS"><?php echo $row->address ?>
                        <td class="option"><a
                                    href="<?php echo admin_url('admin/edit/' . $row->id) ?>"
                                    title="Edit" class="tipS "> <img
                                        src="<?php echo public_url('admin') ?>/images/icons/color/edit.png"/>
                            </a> <a href="<?php echo admin_url('admin/delete/' . $row->id) ?>"
                                    title="Delete" class="tipS verify_action"> <img
                                        src="<?php echo public_url('admin') ?>/images/icons/color/delete.png"/>
                            </a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </form>
    </div>
</div>

<div class="clear mt30"></div>

<!-- head -->
<?php $this->load->view('admin/admin/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Update Admin</h6>
        </div>

        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="<?php echo $action ?>">
            <fieldset>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Username:<span
                                class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"> <?php echo $info->username ?></span>
                        <div class="clear error"><?php echo form_error('username') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Full Name:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name"
                                                    value="<?php echo $info->name ?>" name="name"></span> <span
                                class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Phone:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_phone"
                                                    value="<?php echo $info->phone ?>" name="phone"></span> <span
                                class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('phone') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Address:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_address"
                                                    value="<?php echo $info->address ?>" name="address"></span> <span
                                class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('address') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Email:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text"
                                                    value="<?php echo $info->email ?>" id="param_email"
                                                    name="email"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('email') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Password:<span
                                class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"> <input type="password" id="param_password"
                                                     name="password">
							<p>If updating your password, enter your new value</p>
						</span> <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('password') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formRow">
                    <label for="param_username" class="formLeft">Reply password:<span
                                class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="password" id="param_re_password"
                                                    name="re_password"></span> <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('re_password') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formSubmit">
                    <input type="submit" class="redB" value="Update">
                </div>
            </fieldset>
        </form>

    </div>
</div>

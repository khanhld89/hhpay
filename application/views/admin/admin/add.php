<!-- head -->
<?php $this->load->view('admin/admin/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Add Admin</h6>
        </div>


        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="add">
            <fieldset>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Full Name:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo set_value('name') ?>" name="name" placeholder="Import Full Name"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Phone:<span
                                class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_phone" value="<?php echo set_value('phone') ?>" name="phone" placeholder="Import Phone"></span>
                        <div class="clear error"><?php echo form_error('phone') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Address:<span class="req"></span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_address" value="<?php echo set_value('address') ?>" name="address" placeholder="Import Address"></span>
                        <div class="clear error"><?php echo form_error('address') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_username" class="formLeft">UserName:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" value="<?php echo set_value('username') ?>" id="param_username" name="username" placeholder="Import UserName"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('username') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_username" class="formLeft">Email:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" value="<?php echo set_value('email')?>" id="param_email" name="email" placeholder="Import Email EXP : admin@mhoanghiep.vn" </span>
                        <div class="clear error"><?php echo form_error('email') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="formRow">
                    <label for="param_username" class="formLeft">PassWord:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="password" id="param_password" name="password" placeholder="Enter the password at least 6 characters"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('password') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_username" class="formLeft">Reply PassWord:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type=password id="param_password" name="re_password" placeholder="Enter the password at least 6 characters"></span>
                        <span class="autocheck"></span>
                        <div class="clear error"><?php echo form_error('re_password') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="clear"></div>
                <div class="formSubmit">
                    <input type="submit" class="redB" value="Add">
                </div>

            </fieldset>
        </form>

    </div>
</div>
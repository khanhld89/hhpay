<div class="titleArea">
	<div class="wrapper">
		<div class="pageTitle">
			<h5>Administrators</h5>
			<span>Management Administrator</span>
		</div>
		
		<div class="horControlB menu_action">
			<ul>
				<li><a href="<?php echo admin_url('admin/add')?>">
					<img src="<?php echo public_url('admin')?>/images/icons/control/16/add.png">
					<span>Add Admin</span>
				</a></li>
				
				<li><a href="<?php echo admin_url('admin/index')?>">
					<img src="<?php echo public_url('admin')?>/images/icons/control/16/list.png">
					<span>List Admin</span>
				</a></li>
			</ul>
		</div>
		
		<div class="clear"></div>
	</div>
</div>
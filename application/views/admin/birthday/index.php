<?php $this->load->view('admin/birthday/head', $this->data) ?>
<div class="line"></div>
<div class="wrapper">
    <?php $this->load->view('admin/message', $this->data); ?>
    <div class="widget">

        <div class="title">
            <span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck"/></span>
            <h6>Danh sách đã đăng ký</h6>
            <div class="num f12"> Tổng: <b><?php echo $total ?> bản ghi</b>
            </div>
        </div>

            <table class="sTable mTable myTable" id="checkAll">
                <thead class="filter">
                <tr>
                    <td colspan="8">
                        <form method="get" id="form" action="<?php echo admin_url('birthday') ?>" class="list_filter form">
                            <table width="80%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td style="width:40px;" class="label"><label for="filter_id">Mã số</label></td>
                                    <td class="item"><input type="text" style="width:55px;" id="filter_id" value="<?php echo $this->input->get('id') ?>" name="id">
                                    </td>

                                    <td style="width:40px;" class="label"><label for="filter_name">Name:</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_name" value="<?php echo $this->input->get('name') ?>" name="name"></td>

                                    <td style="width:40px;" class="label"><label for="filter_phone">Sđt</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_phone" value="<?php echo $this->input->get('phone') ?>" name="phone"></td>

                                    <td style="width:40px;" class="label"><label for="filter_email">Email</label></td>
                                    <td style="width:155px;" class="item"><input type="text" style="width:155px;" id="filter_email" value="<?php echo $this->input->get('email') ?>" name="email"></td>

                                    <td style="width:40px;" class="label"><label for="filter_email">Ngày</label></td>
                                    <td style="width:100px;" class="item"><input id="datepicker" type="text" style="width:100px;" id="filter_email" value="<?php echo $this->input->get('create_at') ?>" name="create_at"></td>


                                    <td style="width:150px">
                                        <input type="submit" value="Lọc" class="button blueB">
                                        <input type="reset" onclick="window.location.href = '<?php echo admin_url('birthday') ?>'; " value="Reset" class="basic">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                </thead>
                <thead>
                <tr>
                    <td style="width:10px;"><img src="<?php echo public_url('admin'); ?>/images/icons/tableArrows.png"/>
                    </td>
                    <td style="width: 80px;">ID</td>
                    <td>Họ và tên</td>
                    <td>Số điện thoại</td>
                    <td>Email</td>
                    <td>Thông điệp</td>
                    <td>Thời gian đăng ký</td>
                    <td style="width: 100px;">Action</td>
                </tr>
                </thead>

                <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="9">
                        <div class="list_action itemActions">
                            <a url="<?php echo admin_url('birthday/delete_all') ?>" class="button blueB" id="submit" href="#submit">
                                <span style="color:white;">Delete</span>
                            </a>
                        </div>
                        <div class='pagination'>
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </td>
                </tr>
                </tfoot>


                <tbody class="list_item">
                <?php foreach ($list as $row): ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?php echo $row->id; ?>"/></td>
                        <td class="textC"><?php echo $row->id ?></td>
                        <td><span title="<?php echo $row->name ?>" class="tipS"><?php echo $row->name ?></span></td>
                        <td><span title="<?php echo $row->phone ?>" class="tipS"><?php echo $row->phone ?></span></td>
                        <td><span title="<?php echo $row->email ?>" class="tipS"><?php echo $row->email ?></span></td>
                        <td><span title="<?php echo $row->message ?>" class="tipS"><?php echo $row->message ?></span></td>
                        <td><span class="tipS"><?php echo date("d-m-Y", strtotime($row->create_at)) ?>
                        <td class="option"><a href="<?php echo admin_url('birthday/edit/' . $row->id) ?>" title="Edit" class="tipS "> <img src="<?php echo public_url('admin') ?>/images/icons/color/edit.png"/>
                            </a> <a href="<?php echo admin_url('birthday/delete/' . $row->id) ?>" title="Delete" class="tipS verify_action"> <img src="<?php echo public_url('admin') ?>/images/icons/color/delete.png"/>
                            </a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
    </div>
</div>

<div class="clear mt30"></div>

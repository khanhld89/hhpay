<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Danh sách đăng ký</h5>
            <span>Nhận quà sự kiện 19.000.000 VNĐ</span>
        </div>

        <div class="horControlB menu_action">
            <ul>
                <li><a href="<?php echo admin_url('birthday/add')?>">
                        <img src="<?php echo public_url('admin')?>/images/icons/control/16/add.png">
                        <span>Thêm mới</span></a>
                </li>

                <li><a href="<?php echo admin_url('birthday/index')?>">
                        <img src="<?php echo public_url('admin')?>/images/icons/control/16/list.png">
                        <span>Danh sách</span></a>
                </li>

                <li><a href="<?php echo admin_url('birthday/downloadExcel')?>">
                        <img src="<?php echo public_url('admin')?>/images/icons/control/16/database.png">
                        <span>Xuất Excel</span></a>
                </li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
    (function($)
    {
        $(document).ready(function()
        {
            var main = $('#form');

            // Tabs
            main.contentTabs();
        });
    })(jQuery);
</script>

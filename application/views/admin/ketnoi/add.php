<!-- head -->
<?php $this->load->view('admin/ketnoi/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới thông tin đăng ký</h6>
        </div>


        <form id="form" class="form" enctype="multipart/form-data"
              method="post" action="add">
            <fieldset>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Họ và tên:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_name" value="<?php echo set_value('name') ?>" name="name" placeholder="Nhập họ và tên cần thêm"></span>
                        <div class="clear error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_name" class="formLeft">Số điện thoại:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" id="param_phone" value="<?php echo set_value('phone') ?>" name="phone" placeholder="Nhập số điện thoại cần thêm"></span>
                        <div class="clear error"><?php echo form_error('phone') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_email" class="formLeft">Email:<span
                                class="req">*</span></label>
                    <div class="formRight">
						<span class="oneTwo"><input type="text" value="<?php echo set_value('email')?>" id="param_email" name="email" placeholder="Nhập Email cần thêm.VD: pay@nap.hoanghiep.vn" </span>
                        <div class="clear error"><?php echo form_error('email') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
                <div class="formSubmit">
                    <input type="submit" class="redB" value="Thêm mới">
                </div>

            </fieldset>
        </form>

    </div>
</div>
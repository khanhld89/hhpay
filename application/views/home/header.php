<header class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a id="logo" class="pull-left" href="#"></a>

            <div class="nav-collapse collapse pull-right">
                <ul class="nav">
                    <li class="active"><a href="http://pay.hoanghiep.vn/dich-vu.html" target="__blank">Dịch vụ</a></li>
                    <li><a href="http://developer.hoanghiep.vn/" target="__blank">Developer</a></li>
                    <li><a target="_blank" href="<?php echo home_url()?>/hop-dong.html">Hợp đồng</a></li>
                    <li><a data-toggle="modal" data-target=".myPopUp" href="#">EVENT</a></li>
                    <li class="login">
                        <a data-toggle="modal" href="http://id.hoanghiep.vn/dang-nhap-sso.html?from=cGF5LmhvYW5naGllcC52bg=="><i class="icon-lock"></i> Đăng Nhập</a>
                    </li>
                    <li><a><img src="<?php echo public_url()?>icon/call.png"/><?php echo $config->phone ?></a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</header>
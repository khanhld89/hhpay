<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HOANGHIEP PAYMENT 8 NĂM MỘT CHẶNG ĐƯỜNG</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo public_url()?>css/popup.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url()?>css/page-defaults-5a180f1.css" rel="stylesheet" media="screen" type="text/css"/>
</head>
<body class="lp-pom-body ">
<div class="lp-element lp-pom-root" id="lp-pom-root">
    <div id="lp-pom-root-color-overlay"></div>
    <div class="lp-positioned-content">
        <div class="lp-element lp-pom-box" id="lp-pom-box-10">
            <div id="lp-pom-box-10-color-overlay"></div>
            <div class="lp-element lp-pom-text nlh">
                <p class="lplh-29" style="text-align: center">
                    <span style="font-size:17px;"><span style="font-family:ubuntu; color: #ffffff; ">ĐĂNG KÝ NHẬN NGAY </span></span>
                </p>
                <p class="lplh-10" style="text-align: center">
                    <span style="font-size:35px;"><span style="font-family:ubuntu;"><span style="color:#83ff00;">19.000.000 VNĐ</span></span></span>
                </p>
            </div>
            <div class="lp-element lp-pom-form" id="lp-pom-form-64">
                <form action="#" method="post">
                    <fieldset >
                        <div class="lp-pom-form-field clearfix" id="container_name">
                            <label for="name" class="main lp-form-label" >Họ và tên: </label>
                            <input type="text" name="name" class="text form_elem_name"  value="<?php echo set_value('name') ?>"placeholder="Nhập đầy đủ họ và tên của bạn">
                            <div id="error" ><?php echo form_error('name') ?></div>
                        </div>
                        <div class="lp-pom-form-field clearfix" id="container_email">
                            <label  class="main lp-form-label" >Số điện thoại</label>
                            <input type="text" name="phone" class="text form_elem_phone_number"  value="<?php echo set_value('phone') ?>" placeholder="Nhập số điện thoại của bạn" >
                            <div id="error"><?php echo form_error('phone') ?></div>
                        </div>
                        <div class="lp-pom-form-field clearfix" id="container_phone_number">
                            <label for="email" class="main lp-form-label" >Địa chỉ Email</label>
                            <input type="text" name="email" class="text form_elem_email"  value="<?php echo set_value('email') ?>"  placeholder="Nhập đại chỉ Email của bạn">
                            <div id="error"><?php echo form_error('email') ?></div>
                        </div>
                    </fieldset>
                    <input type="submit" class="lp-element lp-pom-button" id="lp-pom-button-65" value="ĐĂNG KÝ NGAY") onclick="myFunction()">

                </form>

                <script>
                    function myFunction() {
                        alert("Chúc mừng bạn đã nhận đăng ký nhận 19.000.000 Vnđ Thành công!");
                    }
                </script>
            </div>
        </div>
        <div class="lp-element lp-pom-text nlh" id="lp-pom-text-62">
            <p class="lplh-35" style="text-align: center">
                <span style="font-size:22px;"><span style="font-family:ubuntu;"><span style="color:#83ff00;"><b><img
                                    style="width: 33px" src="<?php echo public_url()?>icon/hot-icon.gif"> TỔNG GIÁ TRỊ LÊN ĐẾN 19.000.000 VNĐ <img
                                    style="width: 33px" src="<?php echo public_url()?>icon/hot-icon.gif"> </b></span></span></span>
            </p>
            <p style="text-align: center">
                <span style="color:#ffffff; font-size:12px "><i>(Chỉ cần là Admin Games là bạn có thể tham gia nhận quà từ HoangHiep Payment)</i></span>
            </p>
            <p class="lplh-26">
                <span style="font-size:16px;"><span style="font-family:ubuntu; font-size: 16px"><span
                            style="color:#ffffff;">Nhân dịp kỷ niệm 8 năm thành lập HoangHiep Payment trân trọng gửi tặng 150 suất quà tới các anh em đang làm Games, gói quà tặng trị giá lên tới 19.000.000 VND</span>
                    <ul style="color: #ffffff">
                        <li>Free Gói dịch vụ thiết kế web – Trị giá  <b style="color: #83ff00"> 8.000.000 VND </b></li>
                        <li>Free Gói dịch vụ thiết kế banner 1 năm – Trị giá  <b
                                style="color: #83ff00"> 5.000.000 VND </b></li>
                        <li>Free Gói dịch vụ cho thuê VPS 6 tháng – Trị giá   <b
                                style="color: #83ff00"> 6.000.000 VND </b></li>
                    </ul>
                </span></span>
                <span style="color:#ffffff; font-size: 16px">LƯU Ý: <b>Chỉ</b> dành cho <b>150</b> người đăng ký SỚM NHẤT</span>
            </p>
            <br/>
            <p class="lplh-26">
                <span style="color:#83ff00; font-size: 15px;text-align: center "> <b> HÃY NHANH TAY ĐĂNG KÝ ĐỂ CHỚP LẤY CƠ HỘI LỚN NHẤT NÀY</b></span>
            </p>
            <p>----------------------------------------------------</p>
            <p style="color: #fff;)">
                <span>Hoàng Hiệp Payment</span> <br/>
                <span>🔱 Website: <a style="color: #83ff00;" href="/">Nap.HoangHiep.Vn</a></span><br/>
                <span>🌱 Email: <a style="color: #83ff00;" title="hoanghiep.pay@gmail.com" href="mailto:hoanghiep.pay@gmail.com">hoanghiep.pay@gmail.com</a></span><br/>
                <span>☎  Hotline: <b style="color:#83ff00"> 0941188386 </b></span> <br/>
            </p>
        </div>
    </div>
    <div class="lp-element lp-pom-block" id="lp-pom-block-9">
        <div id="lp-pom-block-9-color-overlay"></div>
        <div class="lp-pom-block-content"></div>
    </div>
</div>
</body>
</html>

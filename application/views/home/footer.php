<section id="bottom" class="main">
    <!--Container-->
    <div class="container">

        <!--row-fluids-->
        <div class="row-fluid">

            <!--Contact Form-->
            <div class="span5">
                <h4>Địa Chỉ</h4>
                <ul class="unstyled address">
                    <li>
                        <i class="icon-home"></i><strong>Address:</strong><?php echo $config->address ?><br>
                    </li>
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> <?php echo $config->email?>
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Website:</strong> <?php echo $config->url_home ?>
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Toll Free:</strong> <?php echo $config->phone_all?>
                    </li>
                </ul>
            </div>
            <!--End Contact Form-->

            <!--Important Links-->
            <div id="tweets" class="span5">
                <h4><?php echo $config->name?></h4>
                <div>
                    <ul class="arrow">
                        <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i>Về Chúng Tôi</a>
                        </li>
                        <li><a href="<?php echo home_url()?>/hop-dong.html" target="_blank" title="Hợp đồng">Hợp đồng sử dụng dịch vụ</a></li>
                        <li><a href="#">Chăm Sóc Khách Hàng</a></li>
                        <li><a href="#">Chiến Lược Kinh Doanh</a></li>
                        <li><a href="#">Copyright Hoàng Hiệp Group 2017</a></li>
                    </ul>
                </div>
            </div>
            <!--Important Links-->
        </div>
        <!--/row-fluid-->
    </div>
    <!--/container-->

</section>

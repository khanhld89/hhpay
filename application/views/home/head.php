<?php
$title      = '';
$meta_key   = '';
$meta_des   = '';
isset($seo_title) ? $title = $seo_title: $title = $config->title;
isset($seo_meta_key) ? $meta_key = $seo_meta_key : $meta_key = $config->meta_key;
isset($seo_meta_des) ? $meta_des = $seo_meta_des : $meta_des = $config->meta_des;
?>
<title><?php echo $title ?></title>
<meta charset="utf-8">
<meta name="description" content="<?php echo $meta_des?>" />
<meta name="keywords" content="<?php echo $meta_key?>" />
<meta name="author" content="<?php echo $config->meta_author?>" />

<link rel="stylesheet" href="<?php echo public_url()?>css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo public_url()?>css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="<?php echo public_url()?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo public_url()?>css/main.css">
<link rel="stylesheet" href="<?php echo public_url()?>css/sl-slide.css">
<link rel="stylesheet" href="<?php echo public_url()?>css/formthe.css">
<link rel="stylesheet" href="<?php echo public_url()?>css/font-awesome.min.css"/>
<link rel="shortcut icon" href="<?php echo public_url()?>images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo public_url()?>images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo public_url()?>images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo public_url()?>images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo public_url()?>images/ico/apple-touch-icon-57-precomposed.png">
<script src="<?php echo public_url()?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="<?php echo public_url()?>js/popup.js"></script>
<script src="<?php echo public_url()?>js/vendor/jquery-1.9.1.min.js"></script>
<script src="<?php echo public_url()?>js/vendor/bootstrap.min.js"></script>
<script src="<?php echo public_url()?>js/main.js"></script>
<!-- Required javascript files for Slider -->
<script src="<?php echo public_url()?>js/jquery.ba-cond.min.js"></script>
<script src="<?php echo public_url()?>js/jquery.slitslider.js"></script>
<!-- /Required javascript files for Slider -->

<!-- SL Slider -->
<script type="text/javascript">
    $(function () {
        var Page = (function () {

            var $navArrows = $('#nav-arrows'),
                slitslider = $('#slider').slitslider({
                    autoplay: true
                }),

                init = function () {
                    initEvents();
                },
                initEvents = function () {
                    $navArrows.children(':last').on('click', function () {
                        slitslider.next();
                        return false;
                    });

                    $navArrows.children(':first').on('click', function () {
                        slitslider.previous();
                        return false;
                    });
                };

            return {init: init};

        })();

        Page.init();
    });
</script>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- show popUp-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#myPopUp").modal('show');
    });
</script>
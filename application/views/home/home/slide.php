<section id="slide-show">
    <div id="slider" class="sl-slider-wrapper">
        <?php if ($slide):
        foreach ($slide as $row):
        ?>
        <div class="sl-slider">
            <div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25"
                 data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <img class="pull-right" src="<?php echo public_url()?>/upload/slide/<?php echo $row->image ?>" alt="<?php echo $row->name ?>"/>
                </div>
            </div>
        </div>
        <?php endforeach;
        endif;
        ?>
        <nav id="nav-arrows" class="nav-arrows">
            <span class="nav-arrow-prev"><i class="icon-angle-left"></i></span>
            <span class="nav-arrow-next"><i class="icon-angle-right"></i></span>
        </nav>

    </div>
</section>
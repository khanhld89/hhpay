<!--slide-->
<?php $this->load->view('home/home/slide'); ?>
<!--popup-->
<?php $this->load->view('home/home/popup'); ?>
<!--end slide-->
<div class="general_social_icons">
    <nav class="social">
        <ul>
            <li class="w3_facebook"><a href="https://www.messenger.com/t/1151170501660425" target="_blank"><img src="<?php echo public_url() ?>icon/facebook2.png"/></a></li>

        </ul>

    </nav>
</div>
<!--Services-->
<section id="services">
    <div class="container">
        <!--Đổi thẻ - mua thẻ - fanapge-->
        <div class="row-fluid">
            <div class="span12">
                <div class="maq" style="font-family: robot;">
                    <marquee>HoangHiep Payment giúp các Admin Games nhà quản lý chỉ cần tập trung tối đa thực hiện công
                        việc chuyên môn của mình, không cần phải lo lắng điều gì khác, mà vẫn nhận được DOANH THU CAO
                        NHẤT với thời gian LINH ĐỘNG NHẤT.
                    </marquee>
                </div>
            </div>
            <div class="span4">
                <div class="media">
                    <div class="media-body">
                        <!--form-->
                        <div class="box-out-wrap">
                            <h4 style="background: #0f4f9e">Đổi thẻ cào thành tiền</h4>
                            <div class="box-out">
                                <form id="frmInputCard" action="/Input/Card" novalidate="novalidate">
                                    <div class="form-group">
                                        <select class="form-control input-validation-error">
                                            <option value="-1">--Chọn Loại Thẻ --</option>
                                            <optgroup>
                                                <option value="3">Viettel</option>
                                                <option value="2">Vinaphone</option>
                                                <option value="1">Mobifone</option>
                                                <option value="67">Gate</option>
                                                <option value="68">Vcoin</option>
                                                <option value="65">Thẻ Megacard</option>
                                                <option value="64">Oncash</option>
                                                <option value="74">Zing</option>
                                                <option value="66">Bit</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="CardSerial" name="CardSerial"
                                               placeholder="Nhập số serial" type="text" value=""">
                                        <p class="validate-mess" data-valmsg-for="CardSerial"
                                           data-valmsg-replace="true"></p>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="CardCode" name="CardCode"
                                               placeholder="Nhập mã thẻ" type="text" value=""">
                                        <p class="validate-mess" data-valmsg-for="CardCode"
                                           data-valmsg-replace="true"></p>
                                    </div>
                                    <div class="box-out-footer">
                                        <p class="validate-mess" data-valmsg-for="Unknow"></p>
                                        <a class="btn btn-primary btn-lg" id="btnInputCard">Đổi thẻ thành tiền</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--end form-->
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="media">
                    <div class="media-body">
                        <!--form-->
                        <div class="box-out-wrap" style="background: #d8f0fa">
                            <h4 style="background: #0f4f9e">Mua Mã Thẻ</h4>
                            <div class="box-out">
                                <form id="frmInputCard" action="/Input/Card" novalidate="novalidate">
                                    <div class="form-group">
                                        <select class="form-control input-validation-error">
                                            <option value="-1">--Chọn loại thẻ--</option>
                                            <optgroup label="Thẻ điện thoại">
                                                <option value="3">Viettel</option>
                                                <option value="2">Vinaphone</option>
                                                <option value="1">Mobifone</option>
                                            </optgroup>
                                            <optgroup label="Thẻ game">
                                                <option value="67">ZING</option>
                                                <option value="68">VCARD</option>
                                                <option value="65">Oncash</option>
                                                <option value="64">Thẻ Megacard</option>
                                                <option value="74">GCard</option>
                                                <option value="66">FPT Gate</option>
                                            </optgroup>
                                        </select>
                                        <select class="form-control input-validation-error">
                                            <option value="-1">--Chọn Mệnh Giá--</option>
                                            <optgroup label="Thẻ điện thoại">
                                                <option value="1">10.000</option>
                                                <option value="2">20.000</option>
                                                <option value="3">50.000</option>
                                                <option value="4">100.000</option>
                                                <option value="4">200.000</option>
                                                <option value="5">500.000</option>
                                        </select>
                                        <select class="form-control input-validation-error">
                                            <option value="-1">--Chọn Phương Thức Thanh Toán--</option>
                                            <option value="1">--Chọn ngân hàng--</option>
                                            <option value="2">Vietcombank</option>
                                            <option value="3">Techcombank</option>
                                            <option value="4">VIB Bank</option>
                                            <option value="5">ABBank</option>
                                            <option value="6">Sacombank</option>
                                            <option value="7">Maritime Bank</option>
                                            <option value="8">Navibank</option>
                                            <option value="9">Vietinbank</option>
                                            <option value="10">DongABank</option>
                                            <option value="11">HDBank</option>
                                            <option value="12">VietABank</option>
                                            <option value="13">VPBank</option>
                                            <option value="14">ACB</option>
                                            <option value="15">MBBank</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="CardSerial" name="CardSerial"
                                               placeholder="Nhập Số Lượng" type="text" value="">
                                        <p class="validate-mess" data-valmsg-for="CardSerial"
                                           data-valmsg-replace="true"></p>
                                    </div>
                                    <div class="box-out-footer">
                                        <p class="validate-mess" data-valmsg-for="Unknow"></p>
                                        <a class="btn btn-primary btn-lg" id="btnInputCard">Mua Thẻ</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--end form-->
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="media">
                    <div class="media-body">
                        <div class="fb-page" data-href="https://www.facebook.com/Ho%C3%A0ng-Hi%E1%BB%87p-Payment-1151170501660425/"
                             data-tabs="timeline" data-height="408px" data-small-header="false"
                             data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/Ho%C3%A0ng-Hi%E1%BB%87p-Payment-1151170501660425/"
                                        class="fb-xfbml-parse-ignore"><a
                                        href="https://www.facebook.com/Ho%C3%A0ng-Hi%E1%BB%87p-Payment-1151170501660425/">Hoàng Hiệp
                                    Payment</a>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--BANG GIA DOI THE-->
        <div class="row-fluid">
            <div class="span12" id="banggia">
                <label class="lbbangia"><h4>BẢNG GIÁ ĐỔI THẺ CÀO THÀNH TIỀN MẶT</h4></label>
                <?php if ($doithe):
                foreach ($doithe as $row):
                ?>
                <div class="span2">
                    <img src="<?php echo public_url() ?>/upload/doi-the/<?php echo $row->image ?>" alt=" <?php echo $row->name?>" class="img-rounded">
                    <div class="caption">
                        <p class="chietkhau"><?php echo $row->ty_le?></p>
                    </div>
                </div>
                <?php endforeach;
                endif;
                ?>
            </div>
        </div>
        <!--FORM ĐĂNG KÝ KẾT NỐI NGAY-->
        <div class="row-fluid">
            <div class="span8">
                <div class="media">
                    <div class="media-body">
                        <!--form-->
                        <div class="box-out-wrap">
                            <h4 style="background: #0f4f9e">KẾT NỐI NẠP THẺ ĐỔI TIỀN NGAY HÔM NAY ĐỂ NHẬN ĐƯỢC Quà tặng tiền mặt <b> 3.999.000 VND </b></h4>
                            <p style="text-align: center"> Chỉ áp dụng cho 99 người đăng ký sớm nhất</p>
                            <div class="box-ketnoi">
                                <form id="frmInputCard" action="home" novalidate="novalidate" method="post">

                                    <div class="form-group">
                                        <input class="form-control" id="CardSerial" name="name" placeholder="Nhập Họ Và Tên" type="text" value="<?php echo set_value('name') ?>">
                                        <div><?php echo form_error('name') ?></div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control" id="CardCode" name="phone" placeholder="Nhập Số Điện Thoại" type="text" value="<?php echo set_value('phone') ?>">
                                        <div><?php echo form_error('phone') ?></div>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control" id="CardCode" name="email" placeholder="Nhập Địa Chỉ Email" type="text" value="<?php echo set_value('email') ?>">
                                        <div><?php echo form_error('email') ?></div>
                                    </div>
                                        <button onclick="myFunction()" class="btn btn-primary btn-lg">ĐĂNG KÝ KẾT NỐI NGAY</button>
                                </form>
                                <script>
                                    function myFunction() {
                                        alert("Chúc mừng bạn đăng ký kết nối nhận quà thành công!");
                                    }
                                </script>
                            </div>
                        </div>
                        <!--end form-->
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="media">
                    <div class="media-body">
                        <img src="<?php echo public_url()?>/upload/slide/banner.png" alt="doi-the" height="340px">
                    </div>
                </div>
            </div>
        </div>
        <!--dau so-->
        <div class="row-fluid">
            <div class="span12">
                <div class="media">
                    <div class="media-body">
                        <div>
                            <table width="100%" border="1"
                                   class="table table-bordered table-striped mT20 table-bordered-green">
                                <tbody>
                                <tr class="bg-lable2">
                                    <td colspan="8" align="center" valign="middle"
                                        class="text-center textCASE fz13 fb clfff pdt10 pdb10">
                                        <p class="text-center" style="margin: 0 0 0px; color: #fff;">
                                            <strong>ĐẦU SỐ</strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <span class="cl0098de"><strong>Đầu số</strong></span>
                                    </td>
                                    <td align="center" valign="middle">
                                        <span class="cl0098de"><strong>Cước phí</strong></span>
                                    </td>
                                    <td align="center" valign="middle">
                                        <span class="cl0098de"><strong>Loại dịch vụ </strong></span>
                                    </td>
                                    <td align="center" valign="middle">
                                        <span class="cl0098de"><strong>Doanh Thu</strong></span>
                                    </td>
                                </tr>
                                <?php if ($dauso):
                                foreach ($dauso as $row):?>
                                <tr>
                                    <td align="left"> <?php echo $row->dau_so?></td>
                                    <td align="center"> <?php echo $row->cuoc_phi?></td>
                                    <td align="center"><?php echo $row->type?></td>
                                    <td align="center"><?php echo $row->ty_le?></td>
                                </tr>
                                <?php endforeach;
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--TopUp-->
        <div class="row-fluid">
            <div class="span12">
                <div class="media">
                    <div class="media-body">
                        <div>
                            <table width="100%" border="1"
                                   class="table table-bordered table-striped mT20 table-bordered-green">
                                <tbody>
                                <tr class="bg-lable2">
                                    <td colspan="8" align="center" valign="middle"
                                        class="text-center textCASE fz13 fb clfff pdt10 pdb10">
                                        <p class="text-center" style="margin: 0 0 0px; color: #fff;">
                                            <strong>CHÍNH SÁCH GIÁ DỊCH VỤ TOPUP</strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <span class="cl0098de"><strong>Dịch vụ</strong></span>
                                    </td>
                                    <?php if ($topup):
                                    foreach ($topup as $row): ?>
                                    <td align="center" valign="middle">
                                        <span class="cl0098de"><strong><?php echo $row->name?> </strong></span>
                                    </td>
                                   <?php endforeach;
                                   endif; ?>
                                </tr>
                                <tr>
                                    <td align="left"> Topup</td>
                                    <?php if ($topup):
                                    foreach ($topup as $row): ?>
                                    <td align="center"><?php echo $row->ty_le ?> </td>
                                    <?php endforeach;
                                    endif; ?>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end form topup-->
    </div>
</section>